import * as XLSX from "xlsx";

// 创建一个包含数据的工作簿对象
// const data: any[][] = [
//   ["Name", "Age", "Country"],
//   ["John", 30, "USA"],
//   ["Alice", 25, "UK"],
//   ["Bob", 35, "Canada"],
// ];

const analyseDatas = (dataList: any[][]): ArrayBuffer => {
  const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(dataList);
  const wb: XLSX.WorkBook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, "Sheet1");
  // 将工作簿对象转换为二进制数据
  const wbout: ArrayBuffer = XLSX.write(wb, {
    type: "buffer",
    bookType: "xlsx",
  });
  return wbout;
};

// 导出Excel文件
export const exportExcel = ({
  filename,
  dataList,
}: {
  filename: string;
  dataList: any[][];
}) => {
  const arrayBuffer = analyseDatas(dataList);
  const blob = new Blob([arrayBuffer], { type: "application/octet-stream" });
  const link = document.createElement("a");
  link.href = URL.createObjectURL(blob);
  link.download = filename;
  link.click();
};
